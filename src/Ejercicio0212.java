

/**
 * Fichero: Ejercicio0312.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0212 {

  String marca;
  String modelo;

  Ejercicio0212() {
    marca = "Seat";
    modelo = "Ibiza";
  }

  Ejercicio0212(String mar, String mod) {
    marca = mar;
    modelo = mod;
  }

  public String show() {
    return marca + " " + modelo;
  }

  public static void main(String[] args) {
    Ejercicio0212 c1 = new Ejercicio0212();
    Ejercicio0212 c2 = new Ejercicio0212("Volvo", "V60");
    System.out.println(c1.show());
    System.out.println(c2.show());
  }
}
/* EJECUCION:
Seat Ibiza
Volvo V60
*/
