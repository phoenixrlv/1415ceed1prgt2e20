/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * Fichero: Ejercicio0306.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */
public class Ejercicio0306 {

  double cambio = 1.36;

  Ejercicio0306() {
    cambio = 1.36;
  }

  Ejercicio0306(double c) {
    cambio = c;
  }

  public double dolaresToEuros(double dol) {
    return dol / cambio;
  }

  public double eurosToDolares(double eur) {
    return eur * cambio;
  }

  public static void main(String[] args) {
    Ejercicio0306 f = new Ejercicio0306(1.36);
    double misdol = 50;
    double miseur = f.dolaresToEuros(misdol);
    System.out.println(misdol);
    System.out.println(miseur);
    System.out.println(f.eurosToDolares(miseur));
  }
}

/* EJECUCION:
 50.0
 36.76470588235294
 50.00000000000001
 */
