
package vista;

import util.*;
import modelo.*;

import java.io.IOException;

/**
 * Fichero: Vista.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-nov-2013
 */
public class Vista {

  public Persona tomaDatos() throws IOException {

    Persona alumno = new Persona();
    Util util = new Util();
    String nombre;
    int edad;

    System.out.println("TOMA DE DATOS");

    System.out.print("Nombre: ");
    nombre = util.pedirString();
    alumno.setNombre(nombre);

    System.out.print("Edad: ");
    edad = util.pedirInt();
    alumno.setEdad(edad);

    return alumno;
  }

  public static void muestraDatos(Persona p1, Persona p2, Persona p3) {
    
    System.out.println("MOSTRANDO DATOS");
    
    System.out.println(p1.getNombre() + " " + p1.getEdad());
    System.out.println(p2.getNombre() + " " + p2.getEdad());
    System.out.println(p3.getNombre() + " " + p3.getEdad());

  }
}
